Ext.util.Functions =
{
	createInterceptor: function( origFn, newFn, scope )
	{
		var method = origFn;
		if( !Ext.isFunction(newFn) )
		{
			return origFn;
		}
		else
		{
			return function()
			{
				var me = this, args = arguments;
				newFn.target = me;
				newFn.method = origFn;
				return (newFn.apply(scope || me || window, args) !== false) ? origFn.apply(me || window, args) : null;
			};
		}
	},

	createDelegate: function( fn, obj, args, appendArgs )
	{
		if( !Ext.isFunction(fn) )
		{
			return fn;
		}
		return function()
		{
			var callArgs = args || arguments;
			if( appendArgs === true )
			{
				callArgs = Array.prototype.slice.call(arguments, 0);
				callArgs = callArgs.concat(args);
			}
			else if( Ext.isNumber(appendArgs) )
			{
				callArgs = Array.prototype.slice.call(arguments, 0);
				// copy arguments first
				var applyArgs =
				[
				    appendArgs, 0
				].concat(args);
				// create method call params
				Array.prototype.splice.apply(callArgs, applyArgs);
				// splice them in
			}
			return fn.apply(obj || window, callArgs);
		};
	},

	defer: function( fn, millis, obj, args, appendArgs )
	{
		fn = Ext.util.Functions.createDelegate(fn, obj, args, appendArgs);
		if( millis > 0 )
		{
			return setTimeout(fn, millis);
		}
		fn();
		return 0;
	},

	createSequence: function( origFn, newFn, scope )
	{
		if( !Ext.isFunction(newFn) )
		{
			return origFn;
		}
		else
		{
			return function()
			{
				var retval = origFn.apply(this || window, arguments);
				newFn.apply(scope || this || window, arguments);
				return retval;
			};
		}
	}
};

Ext.defer = Ext.util.Functions.defer;

Ext.createInterceptor = Ext.util.Functions.createInterceptor;

Ext.createSequence = Ext.util.Functions.createSequence;

Ext.createDelegate = Ext.util.Functions.createDelegate;
