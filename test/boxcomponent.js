Ext.ns('Nts.Module');

Nts.Module.BoxComponentDemo = function( config )
{
	Nts.Module.BoxComponentDemo.superclass.constructor.call(this, config);
};

Ext.extend(Nts.Module.BoxComponentDemo, Ext.BoxComponent,
{
	initComponent: function()
	{
		Ext.apply(this,
		{
			renderTo: 'box',
			html: 'abc',
			height: 400,
			width: 500,
			x: 200,
			y: 200,
			style:
			{
				background: 'red',
				position: 'absolute'
			}
		});

		Nts.Module.BoxComponentDemo.superclass.initComponent.call(this);
	}
});

var demo = new Nts.Module.BoxComponentDemo();

var fn = function()
{
	alert(this);
	demo.setSize(100, 100);

};

var btn = new Ext.Button(
{
	text: 'blabla'
});

// fn.defer(2000, this);
