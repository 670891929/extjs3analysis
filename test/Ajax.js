var Ajax = function()
{
	function serialize( obj )
	{
		var a = [];

		for( var ele in obj )
		{
			var value = obj[ele];

			if( Object.prototype.toString.call(value) === '[object Array]' )
			{
				for( var i = 0; i < value.length; i++ )
				{
					a.push(ele + '=' + encodeURIComponent(value[i]));
				}
			}
			else
			{
				a.push(ele + '=' + encodeURIComponent(value));
			}
		}

		return a.join('&')
	}

	var pub =
	{
		request: function( url, options )
		{
			var emptyFn = function()
			{
			};

			options = options;

			var async = options.async !== false, method = options.method || 'GET', type = options.type || 'text', encode = options.encode || 'UTF-8', timeout = options.timeout || 0, data = options.data || null, success =
			options.success || emptyFn, failure = options.failure || emptyFn, scope = options.scope || this, method = method.toUpperCase();

			if( data && Object.prototype.toString.call(data) === '[object Object]' )
			{
				data = serialize(data);
			}

			var xhr = function()
			{
				try
				{
					return new XMLHttpRequest();
				}
				catch (e)
				{
					try
					{
						return new ActiveXObject('Msxml2.XMLHTTP');
					}
					catch (e)
					{
						failure(null, 'create xhr failed', e);
					}
				}
			}();

			if( !xhr )
			{
				return;
			}

			var isTimeout = false, timer;

			if( async && timeout > 0 )
			{
				timer = setTimeout(function()
				{
					xhr.abort();
					isTimeout = true;
				}, timeout);
			}

			xhr.onreadystatechange = function()
			{
				if( xhr.readyState === 4 && !isTimeout )
				{
					var status = xhr.status, result;

					if( status >= 200 && status < 300 )
					{
						switch( type )
						{
						case 'text':
							result = xhr.responseText;
							break;
						case 'json':
							result = function( str )
							{
								try
								{
									return JSON.parse(str);
								}
								catch (e)
								{
									try
									{
										return (new Function('return ' + str))();
									}
									catch (e)
									{
										failure.call(scope, 'parseJSON error', e);
									}
								}
							}(xhr.responseText);
							break;
						case 'xml':
							result = xhr.responseXML;
							break;

						}

						typeof result !== 'undefined' && success.call(scope, result);
					}
					else if( status === 0 )
					{
						failure.call(scope, xhr, 'request timeout');
					}
					else
					{
						failure.call(scope, xhr.status);
					}

					xhr = null;
				}
			};

			xhr.open(method, url, async);
			method === 'POST' && xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded;charset=' + encode);
			xhr.send(data);

			return xhr;
		}
	};
	return pub;
}();

window.onload = function()
{
	/*
	 * Ajax.request('data.json', { method: 'POST', type: 'text', success: function( text ) { console.log(text); }, failure: function() {
	 * console.log(Array.prototype.slice.call(arguments, 0)); }, data: { username: 'hhstuhacker', age: 12 } });
	 */
};

// Ext.lib.Ajax.request('GET', 'data.json',
// {
// success: function()
// {
// console.log(arguments);
// },
// timeout: 1000,
// scope:
// {
// name: 'abc'
// }
// }, 'username=abc', {
//
// });

/*Ext.Ajax.request(
{
	method: 'GET',
	url: 'data.json',
	callback: function( options, success, response )
	{
		console.log(options, success, response);
	},
	scope:
	{
		name: 'abc'
	},
	timeout: 2000,
	el: 'box'
});*/


var ajax = new Ext.data.Connection(
{
	autoAbort: false,
	listeners:
	{
		'beforerequest': function()
		{
			return false;
		}
	}
});

ajax.request(
{
	method: 'GET',
	url: 'data.json',
	callback: function( options, success, response )
	{
		console.log(options, success, response);
	},
	scope:
	{
		name: 'abc'
	},
	timeout: 2000,
	el: 'box'
});
