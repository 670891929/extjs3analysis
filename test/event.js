var observable = new Ext.util.Observable();

observable.addEvents('work', 'eat', 'drink');

observable.addListener('work', function()
{
	alert('work');
}, window);

observable.addListener('eat', function()
{
	alert('eat');
}, window);

observable.fireEvent('work');