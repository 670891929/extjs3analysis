Ext.QuickTips.init();


new Ext.FormPanel(
{
	title: 'HtmlEditor Plugins Form',
	renderTo: Ext.getBody(),
	width: 950,
	height: 400,
	border: false,
	frame: true,
	items:
	[
	    {
		    hideLabel: true,
		    labelSeparator: '',
		    name: 'description',
		    value: 'The quick brown fox jumps over the fence<br/><img src="training.jpg" width="300" height="200"/>',
		    anchor: '100% 100%',
		    xtype: "htmleditor",
		    // enableFont : false,
		    plugins:
		    [
		        new Ext.ux.form.HtmlEditor.Word(), new Ext.ux.form.HtmlEditor.Divider(), new Ext.ux.form.HtmlEditor.Table(), new Ext.ux.form.HtmlEditor.HR(), new Ext.ux.form.HtmlEditor.IndentOutdent(),
		        new Ext.ux.form.HtmlEditor.SubSuperScript(), new Ext.ux.form.HtmlEditor.RemoveFormat()
		    ]
	    }
	],
	buttons:
	[
	    {
		    text: 'Save'
	    }
	]
});
