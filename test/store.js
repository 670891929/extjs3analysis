var data =
[
    [
        '大漠穷秋', 25, '1985-04-02', '我不告诉你'
    ],
    [
        '漠漠', 26, '1984-04-02', '我不告诉你'
    ],
    [
        '月光漠利亚', 27, '1983-04-02', '我不告诉你'
    ],
    [
        '桃花岛|漠漠', 28, '1982-04-02', '我不告诉你'
    ]
];

var store = new Ext.data.ArrayStore(
{
	fields:
	[
	    {
		    name: 'name'
	    },
	    {
		    name: 'age'
	    },
	    {
		    name: 'date'
	    },
	    {
		    name: 'addr'
	    }
	]
});

store.loadData(data);

console.log(store);

store.filter('name', '漠', false, false);

console.log(store);

store.clearFilter();

store.filterBy(function( record, id )
{
	if( record.get('age') === 25 )
	{
		return true;
	}
	return false;
});

console.log(store);

store.clearFilter();

store.sort('age','desc');

console.log(store);