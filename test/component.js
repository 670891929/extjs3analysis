Ext.ns('Nts.Module');

Nts.Module.ComponentDemo = function( config )
{
	Nts.Module.ComponentDemo.superclass.constructor.call(this, config);
};

Ext.extend(Nts.Module.ComponentDemo, Ext.Component,
{
	initComponent: function()
	{
		Ext.apply(this,
		{
			renderTo: 'box',
			html: 'abc'
		});

		Nts.Module.ComponentDemo.superclass.initComponent.call(this);
	}
});

new Nts.Module.ComponentDemo();