Ext.onReady(function()
{
	new Ext.form.ComboBox(
	{
		mode: 'local',
		triggerAction: 'all',
		store: new Ext.data.Store(
		{
			reader: new Ext.data.JsonReader(
			{
				fields:
				[
				    'display', 'value'
				],

				idProperty: 'value',
				root: 'rows',
				totalProperty: 'totalCount'
			}),
			proxy: new Ext.data.HttpProxy(
			{
				url: 'test.json'
			}),
			autoLoad: true
		}),
		valueField: 'display',
		displayField: 'value',
		renderTo: Ext.getBody(),
		listeners:
		{
			'beforequery': function( ev )
			{
				var combo = ev.combo;
				if( !ev.forceAll )
				{
					combo.collapse();
					var input = ev.query;
					if( combo.store.getCount() > 0 )
					{
						combo.store.filterBy(function( record, id )
						{
							var text = record.get(combo.displayField);
							if( Ext.isEmpty(input, true) ) return true;
							return new RegExp(".*" + input + ".*", 'i').test(text);
						});

						if( combo.store.getCount() > 0 )
						{
							combo.expand();
							combo.restrictHeight();
							if( combo.autoSelect !== false )
							{
								combo.select(0, true);
							}
						}
						else
						{
							combo.store.clearFilter();
							combo.collapse();
						}
					}
					return false;
				}

				return true;
			}
		}
	});

});
