var store = new Ext.data.JsonStore(
{
	data:
	[
	    {
		    name: 'name001',
		    url: 'url001',
		    size: 12,
		    lastmod: new Date()
	    },
	    {
		    name: 'name002',
		    url: 'url002',
		    size: 13,
		    lastmod: new Date()
	    },
	    {
		    name: 'name003',
		    url: 'url003',
		    size: 14,
		    lastmod: new Date()
	    },
	    {
		    name: 'name004',
		    url: 'url004',
		    size: 18,
		    lastmod: new Date()
	    }
	],
	fields:
	[
	    'name', 'url',
	    {
		    name: 'size',
		    type: 'float'
	    },
	    {
		    name: 'lastmod',
		    type: 'date',
		    dateFormat: 'timestamp'
	    }
	],
	autoLoad: true
});

var tpl = new Ext.XTemplate(
    '<tpl for=".">',
        '<div class="thumb-wrap" id="{name}">{name}</div>',
    '</tpl>'
);

new Ext.DataView(
{
	store: store,
	tpl: tpl,
	autoHeight: true,
	multiSelect: true,
	//overClass: 'x-view-over',
	itemSelector: 'div.thumb-wrap',
	emptyText: 'No images to display',
	renderTo: Ext.getBody()
})
