// 覆盖了Store的doRequest方法
Ext.data.MemoryProxy = function( data )
{
	// Must define a dummy api with "read" action to satisfy DataProxy#doRequest and Ext.data.Api#prepare *before* calling super
	var api = {};
	api[Ext.data.Api.actions.read] = true;
	Ext.data.MemoryProxy.superclass.constructor.call(this,
	{
		api: api
	});
	this.data = data;
};

Ext.extend(Ext.data.MemoryProxy, Ext.data.DataProxy,
{
	doRequest: function( action, rs, params, reader, callback, scope, arg )
	{
		// No implementation for CRUD in MemoryProxy. Assumes all actions are 'load'
		params = params || {};
		var result;
		try
		{
			result = reader.readRecords(this.data);
		}
		catch (e)
		{
			// @deprecated loadexception
			this.fireEvent("loadexception", this, null, arg, e);

			this.fireEvent('exception', this, 'response', action, arg, null, e);
			callback.call(scope, null, arg, false);
			return;
		}
		callback.call(scope, result, arg, true);
	}
});