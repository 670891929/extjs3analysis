Ext.data.DataReader = function( meta, recordType )
{
	// 元数据，控制该解析的数据
	this.meta = meta;

	// 传入的是数据，即表示定义了每列字段。创建为记录类型
	// [{name:'username',type:'string'},{name:'age',type;'int'}]
	// or Ext.data.Record.create([{name:'username',type:'string'},{name:'age',type;'int'}])
	this.recordType = Ext.isArray(recordType) ? Ext.data.Record.create(recordType) : recordType;

	// if recordType defined make sure extraction functions are defined
	if( this.recordType )
	{
		// 由子类去完成
		this.buildExtractors();
	}
};

Ext.data.DataReader.prototype =
{
	getTotal: Ext.emptyFn,
	getRoot: Ext.emptyFn,
	getMessage: Ext.emptyFn,
	getSuccess: Ext.emptyFn,
	getId: Ext.emptyFn,
	buildExtractors: Ext.emptyFn,
	extractValues: Ext.emptyFn,

	realize: function( rs, data )
	{
		if( Ext.isArray(rs) )
		{
			for( var i = rs.length - 1; i >= 0; i-- )
			{
				// recurse
				if( Ext.isArray(data) )
				{
					this.realize(rs.splice(i, 1).shift(), data.splice(i, 1).shift());
				}
				else
				{
					// weird...rs is an array but data isn't?? recurse but just send in the whole invalid data object.
					// the else clause below will detect !this.isData and throw exception.
					this.realize(rs.splice(i, 1).shift(), data);
				}
			}
		}
		else
		{
			// If rs is NOT an array but data IS, see if data contains just 1 record. If so extract it and carry on.
			if( Ext.isArray(data) && data.length == 1 )
			{
				data = data.shift();
			}
			if( !this.isData(data) )
			{
				// TODO: Let exception-handler choose to commit or not rather than blindly rs.commit() here.
				// rs.commit();
				throw new Ext.data.DataReader.Error('realize', rs);
			}
			rs.phantom = false; // <-- That's what it's all about
			rs._phid = rs.id; // <-- copy phantom-id -> _phid, so we can remap in Store#onCreateRecords
			rs.id = this.getId(data);
			rs.data = data;

			rs.commit();
			rs.store.reMap(rs);
		}
	},

	update: function( rs, data )
	{
		if( Ext.isArray(rs) )
		{
			for( var i = rs.length - 1; i >= 0; i-- )
			{
				if( Ext.isArray(data) )
				{
					this.update(rs.splice(i, 1).shift(), data.splice(i, 1).shift());
				}
				else
				{
					// weird...rs is an array but data isn't?? recurse but just send in the whole data object.
					// the else clause below will detect !this.isData and throw exception.
					this.update(rs.splice(i, 1).shift(), data);
				}
			}
		}
		else
		{
			// If rs is NOT an array but data IS, see if data contains just 1 record. If so extract it and carry on.
			if( Ext.isArray(data) && data.length == 1 )
			{
				data = data.shift();
			}
			if( this.isData(data) )
			{
				rs.data = Ext.apply(rs.data, data);
			}
			rs.commit();
		}
	},

	// 准备数据（若有root，则去取data[rows]的数据），然后遍历数据，通过映射转化为record，加进rs中
	extractData: function( root, returnRecords )
	{
		// A bit ugly this, too bad the Record's raw data couldn't be saved in a common property named "raw" or something.
		var rawName = (this instanceof Ext.data.JsonReader) ? 'json' : 'node';

		var rs = [];

		if( this.isData(root) && !(this instanceof Ext.data.XmlReader) )
		{
			root =
			[
				root
			];
		}
		var f = this.recordType.prototype.fields, // f -->> field
		fi = f.items, // -->> field items
		fl = f.length, // -->> field length
		rs = []; // rs -->> records
		if( returnRecords === true )
		{
			var Record = this.recordType;
			for( var i = 0; i < root.length; i++ )
			{
				var n = root[i];
				var record = new Record(this.extractValues(n, fi, fl), this.getId(n));
				record[rawName] = n; // <-- There's implementation of ugly bit, setting the raw record-data. -->> record['json'] = data;
				rs.push(record);
			}
		}
		else
		{
			for( var i = 0; i < root.length; i++ )
			{
				var data = this.extractValues(root[i], fi, fl);
				data[this.meta.idProperty] = this.getId(root[i]);
				rs.push(data);
			}
		}
		return rs;
	},

	/**
	 * Returns true if the supplied data-hash <b>looks</b> and quacks like data. Checks to see if it has a key corresponding to idProperty defined in your
	 * DataReader config containing non-empty pk.
	 * 
	 * @param {Object} data
	 * @return {Boolean}
	 */
	isData: function( data )
	{
		return (data && Ext.isObject(data) && !Ext.isEmpty(this.getId(data))) ? true : false;
	},

	// private function a store will createSequence upon
	onMetaChange: function( meta )
	{
		delete this.ef;
		this.meta = meta;
		this.recordType = Ext.data.Record.create(meta.fields);
		this.buildExtractors();
	}
};

/**
 * @class Ext.data.DataReader.Error
 * @extends Ext.Error General error class for Ext.data.DataReader
 */
Ext.data.DataReader.Error = Ext.extend(Ext.Error,
{
	constructor: function( message, arg )
	{
		this.arg = arg;
		Ext.Error.call(this, message);
	},
	name: 'Ext.data.DataReader'
});
Ext.apply(Ext.data.DataReader.Error.prototype,
{
	lang:
	{
		'update': "#update received invalid data from server.  Please see docs for DataReader#update and review your DataReader configuration.",
		'realize': "#realize was called with invalid remote-data.  Please see the docs for DataReader#realize and review your DataReader configuration.",
		'invalid-response': "#readResponse received an invalid response from the server."
	}
});
