Ext.data.Field = Ext.extend(Object,
{
	constructor: function( config )
	{
		if( Ext.isString(config) )
		{
			config =
			{
				name: config
			};
		}
		Ext.apply(this, config);

		var types = Ext.data.Types, st = this.sortType, t;

		if( this.type )
		{
			if( Ext.isString(this.type) )
			{
				this.type = Ext.data.Types[this.type.toUpperCase()] || types.AUTO;
			}
		}
		else
		{
			this.type = types.AUTO;
		}

		// named sortTypes are supported, here we look them up
		if( Ext.isString(st) )
		{
			this.sortType = Ext.data.SortTypes[st];
		}
		else if( Ext.isEmpty(st) )
		{
			this.sortType = this.type.sortType;
		}

		if( !this.convert )
		{
			this.convert = this.type.convert;
		}
	},

	dateFormat: null,
	useNull: false,
	defaultValue: "",
	mapping: null,
	sortType: null,
	sortDir: "ASC",
	allowBlank: true
});
