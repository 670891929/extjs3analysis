Ext.data.ArrayReader = Ext.extend(Ext.data.JsonReader,
{
	readRecords: function( o )
	{
		this.arrayData = o;
		var s = this.meta, 
		sid = s ? Ext.num(s.idIndex, s.id) : null, 
		recordType = this.recordType, 
		fields = recordType.prototype.fields, 
		records = [], 
		success = true, v;

		var root = this.getRoot(o);

		//[['hhstuhacker',11,'asdfasfasdfad'],['hhstuhacker',11,'asdfasfasdfad'],['hhstuhacker',11,'asdfasfasdfad']]
		for( var i = 0, len = root.length; i < len; i++ )
		{
			var n = root[i], values = {}, id = ((sid || sid === 0) && n[sid] !== undefined && n[sid] !== "" ? n[sid] : null);
			
			//fields --->> [{field}]
			
			for( var j = 0, jlen = fields.length; j < jlen; j++ )
			{
				var f = fields.items[j], k = f.mapping !== undefined && f.mapping !== null ? f.mapping : j;
				v = n[k] !== undefined ? n[k] : f.defaultValue;
				v = f.convert(v, n);
				values[f.name] = v;
			}
			var record = new recordType(values, id);
			record.json = n;
			records[records.length] = record;
		}

		var totalRecords = records.length;

		if( s.totalProperty )
		{
			v = parseInt(this.getTotal(o), 10);
			if( !isNaN(v) )
			{
				totalRecords = v;
			}
		}
		if( s.successProperty )
		{
			v = this.getSuccess(o);
			if( v === false || v === 'false' )
			{
				success = false;
			}
		}

		var ret =
		{
			success: success,
			records: records,
			totalRecords: totalRecords
		};

		return ret;
	}
});