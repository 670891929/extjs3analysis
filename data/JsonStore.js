// 仅仅换掉了Store中的reader的配置
Ext.data.JsonStore = Ext.extend(Ext.data.Store,
{
	constructor: function( config )
	{
		Ext.data.JsonStore.superclass.constructor.call(this, Ext.apply(config,
		{
			reader: new Ext.data.JsonReader(config)
		}));
	}
});
Ext.reg('jsonstore', Ext.data.JsonStore);