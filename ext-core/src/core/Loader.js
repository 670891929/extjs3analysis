Ext.Loader = Ext.apply({},
{
	load: function( fileList, callback, scope, preserveOrder )
	{
		var scope = scope || this, head = document.getElementsByTagName("head")[0], fragment = document.createDocumentFragment(), numFiles = fileList.length, loadedFiles = 0, me = this;

		// 按照fileList中的索引位置来把script标签加到head里，onFileLoaded是回调函数
		var loadFileIndex = function( index )
		{
			head.appendChild(me.buildScriptTag(fileList[index], onFileLoaded));
		};

		var onFileLoaded = function()
		{
			// 已经load的文件的个数
			loadedFiles++;

			// 如果是最后一个文件，那么调用传入的callback
			if( numFiles == loadedFiles && typeof callback == 'function' )
			{
				callback.call(scope);
			}
			else
			{
				// 如果不是最后一个，并且preserveOrder设置为true，那么就load下一个
				if( preserveOrder === true )
				{
					loadFileIndex(loadedFiles);
				}
			}
		};

		// 如果preserveOrder设置为true，就load第一个
		if( preserveOrder === true )
		{
			loadFileIndex.call(this, 0);
		}
		else
		{

			// 如果preserveOrder设置为false，那么就把所有的都加到fragment

			Ext.each(fileList, function( file, index )
			{
				fragment.appendChild(this.buildScriptTag(file, onFileLoaded));
			}, this);

			head.appendChild(fragment);
		}
	},

	buildScriptTag: function( filename, callback )
	{
		var script = document.createElement('script');
		script.type = "text/javascript";
		script.src = filename;

		// IE has a different way of handling &lt;script&gt; loads, so we need to check for it here
		// IE用script标签的readyState的状态是否为loaded或者complete来判定是否加载完成
		if( script.readyState )
		{
			script.onreadystatechange = function()
			{
				if( script.readyState == "loaded" || script.readyState == "complete" )
				{
					script.onreadystatechange = null;
					callback();
				}
			};
		}
		else
		{
			script.onload = callback;
		}

		return script;
	}
});
