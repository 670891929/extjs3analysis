Ext.util.DelayedTask = function( fn, scope, args )
{
	var me = this, id, call = function()
	{
		clearInterval(id);
		id = null;

		// 这里才是真正的执行
		fn.apply(scope, args || []);
	};

	me.delay = function( delay, newFn, newScope, newArgs )
	{
		me.cancel(); // 如果现在有正在执行的任务，则停掉

		fn = newFn || fn;
		scope = newScope || scope;
		args = newArgs || args;

		// 上面这3句，是兼容构造函数中传入
		id = setInterval(call, delay);
	};

	me.cancel = function()
	{
		if( id )
		{
			clearInterval(id);
			id = null;
		}
	};
};

// //1

var task = new Ext.util.DelayedTask();

task.delay(1000, function()
{
}, this,
[
    1, 2, 3, 4
]);

// //2

var task = new Ext.util.DelayedTask(function()
{
}, this,
[
    1, 2, 3, 4
]);

task.delay();
