Ext.applyIf(Number.prototype,
{
	constrain: function( min, max )
	{
		return Math.min(Math.max(this, min), max);
	}
});