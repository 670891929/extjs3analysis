Ext.apply(Function.prototype,
{
	createInterceptor: function( fcn, scope )
	{
		var method = this;
		// 如果fcn不是函数，则时间返回该函数，
		// 如果是函数，那么。。。。
		// 如果fcn返回值为false，则停止运行this函数
		return !Ext.isFunction(fcn) ? this : function()
		{
			var me = this, args = arguments;
			fcn.target = me;
			fcn.method = method;
			return (fcn.apply(scope || me || window, args) !== false) ? method.apply(me || window, args) : null;
		};
	},

	createCallback: function(/* args... */)
	{
		var args = arguments, method = this;
		return function()
		{
			return method.apply(window, args);
		};
	},

	createDelegate: function( obj, args, appendArgs )
	{
		var method = this;
		return function()
		{
			var callArgs = args || arguments;

			// appendArgs若为boolean，的含义为：将arguments的所有，都添加到args里面去
			if( appendArgs === true )
			{
				callArgs = Array.prototype.slice.call(arguments, 0);
				callArgs = callArgs.concat(args);
			}

			else if( Ext.isNumber(appendArgs) )
			{
				callArgs = Array.prototype.slice.call(arguments, 0); // copy arguments first
				var applyArgs =
				[
				    appendArgs, 0
				].concat(args); // create method call params

				Array.prototype.splice.apply(callArgs, applyArgs); // splice them in
			}
			return method.apply(obj || window, callArgs);
		};
	},

	defer: function( millis, obj, args, appendArgs )
	{
		var fn = this.createDelegate(obj, args, appendArgs);
		if( millis > 0 )
		{
			return setTimeout(fn, millis);
		}
		fn();
		return 0;
	},
	createSequence: function( fcn, scope )
	{
		var method = this;
		return (typeof fcn != 'function') ? this : function()
		{
			var retval = method.apply(this || window, arguments);
			fcn.apply(scope || this || window, arguments);
			return retval;
		};
	}
});
// ////////////////////////////////////////////////////////////////////
var fn = function()
{
	console.log(this);
};

setInterval(fn.createCallback(), 1000);