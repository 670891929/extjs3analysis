Ext.applyIf(String,
{
	format: function( format )
	{
		var args = Ext.toArray(arguments, 1);
		return format.replace(/\{(\d+)\}/g, function( m, i )
		{
			return args[i];
		});
	},
	escape: function( string )
	{
		return string.replace(/('|\\)/g, "\\$1");
	},

	leftPad: function( val, size, ch )
	{
		var result = String(val);
		if( !ch )
		{
			ch = " ";
		}
		while(result.length < size)
		{
			result = ch + result;
		}
		return result;
	},
	toggle: function( value, other )
	{
		return this == value ? other : value;
	},
	trim: function()
	{
		var re = /^\s+|\s+$/g;
		return function()
		{
			return this.replace(re, '');
		};
	}()
});