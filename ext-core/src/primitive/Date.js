Date.prototype.getElapsed = function( date )
{
	return Math.abs((date || new Date()).getTime() - this.getTime());
};