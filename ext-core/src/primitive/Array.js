Ext.applyIf(Array.prototype,
{
	indexOf: function( o, from )
	{
		var len = this.length;
		from = from || 0;
		from += (from < 0) ? len : 0;
		for( ; from < len; ++from )
		{
			if( this[from] === o )
			{
				return from;
			}
		}
		return -1;
	},

	remove: function( o )
	{
		var index = this.indexOf(o);
		if( index != -1 )
		{
			this.splice(index, 1);
		}
		return this;
	}
});