Ext.layout.FitLayout = Ext.extend(Ext.layout.ContainerLayout,
{
	// private
	monitorResize: true,

	type: 'fit',

	getLayoutTargetSize: function()
	{
		var target = this.container.getLayoutTarget();
		if( !target )
		{
			return {};
		}
		// Style Sized (scrollbars not included)
		return target.getStyleSize();
	},

	// private
	onLayout: function( ct, target )
	{
		Ext.layout.FitLayout.superclass.onLayout.call(this, ct, target);
		if( !ct.collapsed )
		{
			this.setItemSize(this.activeItem || ct.items.itemAt(0), this.getLayoutTargetSize());
		}
	},

	// private
	setItemSize: function( item, size )
	{
		if( item && size.height > 0 )
		{ // display none?
			item.setSize(size);
		}
	}
});
Ext.Container.LAYOUTS['fit'] = Ext.layout.FitLayout;